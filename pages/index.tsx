// External dependencies
import React from 'react';
import { GetStaticProps } from 'next';

// Internal modules
import HomeLayout from '@/layouts/home';
import { getDynamicRouteData } from '@/data/paths';
import { LayoutProps } from '@/typings/LayoutProps';
import { getLocalCacheData, prepareLocalCache } from '@/data/cache';

// Get data during buid to pass as props to the page
export const getStaticProps: GetStaticProps = async () => {
	await prepareLocalCache();
	const localCache = await getLocalCacheData();
	const route = await getDynamicRouteData([]);
	const menus = localCache.menus;
	const options = await localCache.options;

	return {
		props: {
			route,
			menus,
			options,
		},
	};
};

function HomePage(props: LayoutProps): JSX.Element {
	return <HomeLayout {...props} />;
}

export default HomePage;
