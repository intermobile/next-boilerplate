// External dependencies
import React from 'react';
import { GetServerSideProps, GetServerSidePropsContext } from 'next';
import { CgInfo } from 'react-icons/cg';

// Internal modules
import DefaultLayout from '@/layouts/default';
import PostLayout from '@/layouts/post';
import { Site } from '@/config/site';
import { RouteProps } from '@/typings/RouteProps';
import { CacheProps } from '@/typings/CacheProps';
import { LayoutProps } from '@/typings/LayoutProps';
import { getCookieValue } from '@/helpers/string';
import { getAssetPath } from '@/helpers/assets';
import { getPostRouteData } from '@/data/posts';

// Get data during build to pass as props to the page
export const getServerSideProps: GetServerSideProps = async (
	context: GetServerSidePropsContext,
) => {
	const previewToken = getCookieValue('wp_preview_token', context.req.headers.cookie);
	if (!context.query.post || !previewToken) return { notFound: true };

	let route: RouteProps = null;
	const postId = parseInt(context.query.post as string);
	route = await getPostRouteData(postId, previewToken);

	if (!route) return { props: {} };

	const host = context.req.headers.host;
	const protocol = host ? (host.indexOf('localhost') > -1 ? 'http://' : 'https://') : '';
	const path = Site.cacheFilePath.replace(/^\.\/public/, '');
	// const path = '/' + Site.slug + Site.cacheFilePath.replace(/^\.\/public/, ''); // FOR MULTUSITE
	const cacheRes = await fetch(`${protocol}${host}${path}`);
	const localCache: CacheProps = await cacheRes.json();
	const options = await localCache.options;
	const menus = await localCache.menus;

	options.mode = 'preview';

	return {
		props: {
			route,
			menus,
			options,
		},
	};
};

function DynamicPage(props: LayoutProps): JSX.Element {
	if (!props.route) {
		return (
			<div
				style={{ height: '100vh' }}
				className="d-flex align-items-center justify-content-center flex-column"
			>
				<h1 className="h2 mb-5">Your preview session has expired</h1>
				<p className="mb-2">
					Click on a <b>Preview</b> link on the CMS to start a new session.
				</p>
				<p className="mb-6">
					<CgInfo className="mr-1" />
					<small>Preview sessions last for a few hours.</small>
				</p>
				<img
					src={getAssetPath(`/images/next-boilerplate-logo.svg`)}
					alt={Site.title}
					width="180"
					className="mb-5"
				/>
			</div>
		);
	}

	switch (props.route.layout) {
		case 'post':
			return <PostLayout {...props} />;
		default:
			return <DefaultLayout {...props} />;
	}
}

export default DynamicPage;
