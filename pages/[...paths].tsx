// External dependencies
import React from 'react';
import { GetStaticPaths, GetStaticProps } from 'next';

// Internal modules
import { Site } from '@/config/site';
import { getDynamicRouteData } from '@/data/paths';
import { getLocalCacheData, prepareLocalCache } from '@/data/cache';
import { LayoutProps } from '@/typings/LayoutProps';

// Layouts
import DefaultLayout from 'layouts/default';
import PostLayout from '@/layouts/post';
import CategoryLayout from '@/layouts/category';
import SubcategoryLayout from '@/layouts/subcategory';

// Define the pages to be generated
export const getStaticPaths: GetStaticPaths = async () => {
	await prepareLocalCache();
	const localCache = await getLocalCacheData();
	const posts = localCache.paths.posts;
	const pages = localCache.paths.pages;
	const categories = localCache.paths.categories;

	let routes: Array<string[]> = [];
	routes = routes.concat(pages.slice(0, Site.limitPages));
	routes = routes.concat(categories.slice(0, Site.limitCategories));
	routes = routes.concat(posts.slice(0, Site.limitPosts));

	const paths = routes.map((path) => {
		return { params: { paths: path } };
	});

	return {
		paths: paths,
		fallback: false,
	};
};

// Get data during buid to pass as props to the page
export const getStaticProps: GetStaticProps = async (context) => {
	const path = context.params.paths as string[];
	const localCache = await getLocalCacheData();
	const route = await getDynamicRouteData(path);
	const menus = localCache.menus;
	const options = await localCache.options;

	return {
		props: {
			route,
			menus,
			options,
		},
	};
};

function DynamicPage(props: LayoutProps): JSX.Element {
	switch (props.route.layout) {
		case 'post':
			return <PostLayout {...props} />;
		case 'category':
			return <CategoryLayout {...props} />;
		case 'subcategory':
			return <SubcategoryLayout {...props} />;
		default:
			return <DefaultLayout {...props} />;
	}
}

export default DynamicPage;
