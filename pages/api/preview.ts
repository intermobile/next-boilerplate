import { getInfoPageHtml, getPreviewData } from '@/helpers/preview';
import type { NextApiRequest, NextApiResponse } from 'next';

export default async (req: NextApiRequest, res: NextApiResponse) => {
	// Check the secret and parameters
	if (!req.query.token || !req.query.id) {
		return res
			.status(401)
			.end(getInfoPageHtml('Invalid Request', 'The requested URL is not valid.'));
	}

	const id = parseInt(req.query.id as string);
	const type = req.query.type as string;
	const token = decodeURIComponent(req.query.token as string);

	const post = await getPreviewData(id, type, token);
	if (!post) {
		return res
			.status(401)
			.end(
				getInfoPageHtml(
					'Invalid Request',
					'The requested content was not found or authorized.',
				),
			);
	}

	res.setHeader('Set-Cookie', [
		`wp_preview_token=${token}; path=/; max-age=${60 * 60 * 24 * 7};`, // 1 week
	]);

	// res.redirect(`/${Site.slug}/preview/?post=${post.id}`);  // FOR MULTISITE
	res.redirect(`/preview/?post=${post.id}`);
};
