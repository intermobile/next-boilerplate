import { createGlobalStyle } from 'styled-components';

export const iconsVersionHash = '27373321'; // Updated automatically with `gulp iconfont`

export const icons = {
	arrow: '\\eA01',
	baby: '\\eA02',
	brain: '\\eA03',
	caret: '\\eA04',
	close: '\\eA05',
	facebook: '\\eA07',
	facebookF: '\\eA06',
	instagram: '\\eA08',
	mail: '\\eA09',
	menu: '\\eA0A',
	messenger: '\\eA0B',
	minus: '\\eA13',
	pestle: '\\eA0C',
	pinterest: '\\eA0D',
	plus: '\\eA14',
	search: '\\eA0E',
	sun: '\\eA0F',
	twitter: '\\eA10',
	whatsapp: '\\eA11',
	youtube: '\\eA12',
};

export const Icons = createGlobalStyle`
  @font-face {
		font-family: 'Icons';
		src: url('/fonts/icons/icons.woff2?v=${iconsVersionHash}') format('woff2'),
			url('/fonts/icons/icons.woff?v=${iconsVersionHash}') format('woff'),
			url('/fonts/icons/icons.ttf?v=${iconsVersionHash}') format('truetype');
		font-weight: normal;
		font-style: normal;
		font-display: swap;
	}

	[class^='icon-'],
	[class*=' icon-'] {
		/* Use !important to prevent issues with browser extensions that change fonts */
		font-family: 'Icons' !important;
		speak: none;
		font-style: normal;
		font-weight: normal;
		font-variant: normal;
		text-transform: none;
		line-height: 1;

		/* Better Font Rendering */
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}

	${Object.entries(icons)
		.map((icon) => {
			return `.icon-${icon[0]}:before {content:"${icon[1]}"}`;
		})
		.join('')}
`;
