import React from 'react';
import { useRouter } from 'next/router';

// Internal modules
import { Component, Content } from './Page.styles';
import { LayoutProps } from '@/typings/LayoutProps';
import { Site } from '@/config/site';
import Head from '@/components/head';
import Header from '@/components/header';
import Footer from '@/components/footer';
import AnalyticsScripts from '@/components/analytics-scripts';
import GuideContainer from '@/components/guide-container';
import { PreviewBanner } from '../preview-banner/PreviewBanner';

// Typings
interface Props extends LayoutProps {
	className?: string;
	children?: React.ReactNode;
}

export const Page = ({ route, options, menus, className, children }: Props): JSX.Element => {
	const router = useRouter();

	return (
		<>
			<Head seo={route.seo} />

			{Site.isAnalyticsEnabled && <AnalyticsScripts />}

			<Component id="page" className={className}>
				<Header menu={menus.find(({ location }) => location === 'header')} />

				<Content id="content">{children}</Content>

				<Footer menus={menus} options={options} />

				{/* CONTAINER GUIDE (For development purposes) */}
				{process.env.NODE_ENV === 'development' && router.query.guide && <GuideContainer />}

				{options.mode === 'preview' && <PreviewBanner />}
			</Component>
		</>
	);
};
