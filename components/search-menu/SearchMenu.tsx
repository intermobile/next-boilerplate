// External dependencies
import React from 'react';
import { Row } from 'react-bootstrap';

// Internal dependencies
import { SearchMenuProps } from './SearchMenu.types';
import { Component } from './SearchMenu.styles';
import { useSearchMenu } from 'utils/hooks/SearchMenuProvider';
import SearchBar from '@/components/search-bar';

// Props for component functionality and logic
interface Props extends SearchMenuProps {
	className?: string;
}

export const SearchMenu: React.FC<Props> = ({ className = '' }) => {
	function onSearch() {
		controlSearchMenu.closeSearchMenu();
	}

	const controlSearchMenu = useSearchMenu();
	return (
		<Component $isActive={controlSearchMenu.isSearchMenuOpen} className={`${className}`}>
			<Row className="m-0 p-0">
				<SearchBar
					className="w-90 my-5"
					onSearch={() => {
						onSearch();
					}}
				/>
			</Row>
		</Component>
	);
};
