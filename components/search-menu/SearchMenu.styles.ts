import styled, { css } from 'styled-components';

interface SearchMenuProps {
	$isActive: boolean;
}

export const Component = styled.div<SearchMenuProps>`
	position: fixed;
	left: 0;
	width: 100vw;
	z-index: 1000;
	overflow: hidden;

	background-color: ${({ theme }) => theme.colors.light};
	transition: all 0.5s ease;

	${(props) =>
		props.$isActive === true
			? css`
					opacity: 1;
					height: 100vh;
			  `
			: css`
					opacity: 0;
					height: 0;
			  `}
`;
