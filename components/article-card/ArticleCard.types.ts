import { ArticleProps } from '@/typings/ArticleProps';

export interface ArticleCardProps {
	post: ArticleProps;
}
