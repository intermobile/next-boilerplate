// External dependencies
import React from 'react';
import Image from 'next/image';

// Internal dependencies
import { Component, Picture, Title, Excerpt, Button } from './ArticleCard.styles';

// Typing variables
import { ArticleCardProps } from './ArticleCard.types';

interface Props extends ArticleCardProps {
	className?: string;
}

export const ArticleCard = ({ post, className }: Props): JSX.Element => {
	return (
		<Component className={`${className} position-relative`}>
			{/* IMAGE */}
			{post.image && (
				<Picture>
					<Image
						src={post.image.src}
						alt={post.image.alt}
						width={post.image.width}
						height={post.image.height}
						objectFit="cover"
					/>
				</Picture>
			)}
			<div className={``}>
				{/* TITLE */}
				<Title className={``}>{post.title}</Title>

				{/* EXCERPT */}
				{post.excerpt && <Excerpt className={``}>{post.excerpt}</Excerpt>}

				{/* BUTTON */}
				{/* Convert to i18n text */}
				<Button href={post.path} className={`btn btn-primary stretched-link`}>
					Read More
				</Button>
			</div>
		</Component>
	);
};
