// External dependencies
import React, { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { Container, Row } from 'react-bootstrap';
import { BsChevronDown } from 'react-icons/bs';

// Internal dependencies
import { getAssetPath } from '@/helpers/assets';
import { Site } from '@/config/site';
import SearchBar from '@/components/search-bar';
import MobileMenu from '@/components/mobile-menu';
import SearchMenu from '@/components/search-menu';

//Hooks
import { useMobileMenu } from 'utils/hooks/MobileMenuProvider';
import { useSearchMenu } from 'utils/hooks/SearchMenuProvider';

// Styles
import {
	ItemMenu,
	MenuList,
	Component,
	HeaderLogo,
	HeaderContent,
	SidebarButton,
	AccessibilityButton,
} from './Header.styles';

// Typing
import { MenuProps } from '@/typings/MenuProps';
interface Props {
	menu: MenuProps;
	className?: string;
}

export const Header: React.FC<Props> = ({ menu, className }) => {
	const router = useRouter();
	const controlSidebar = useMobileMenu();
	const controlSearchMenu = useSearchMenu();

	const [isHome] = useState(() => router.pathname === '/');

	const handleOpenSubmenuHeader = (target) => {
		const menu = document.querySelector('.menu-' + target);
		menu.classList.toggle('active');
		menu.addEventListener('mouseout', () => {
			menu.classList.remove('active');
		});
	};

	return (
		<>
			<AccessibilityButton onClick={() => document.getElementById('content').focus()}>
				Go to page content {/* TODO: Convert to i18n text */}
			</AccessibilityButton>

			<Component className={`${className} w-100`}>
				<Container>
					<HeaderContent className="d-flex align-items-center justify-content-between">
						{/* LOGO */}
						<div className="d-flex order-2 order-lg-1">
							{isHome ? (
								<h1 className="m-0">
									<Link href="/">
										<a>
											<HeaderLogo
												src={getAssetPath(`/images/next-boilerplate-logo.svg`)}
												alt={Site.title}
												width="595"
												height="265"
											/>
										</a>
									</Link>
								</h1>
							) : (
								<div>
									<Link href="/">
										<a>
											<HeaderLogo
												src={getAssetPath(`/images/next-boilerplate-logo.svg`)}
												alt={Site.title}
												width="595"
												height="265"
											/>
										</a>
									</Link>
								</div>
							)}
						</div>

						{/* NAV MENU */}
						<div className="align-items-center justify-content-end order-1 order-lg-2 row">
							<MenuList className="col menu-list-header mb-0 justify-content-between d-none d-lg-flex mr-3">
								{menu.items.map((menuItem, index) => (
									<ItemMenu
										key={index}
										className={`menu-${index} d-flex align-items-center position-relative mx-4`}
									>
										<Link href={menuItem.href}>
											<a
												className="p-2"
												dangerouslySetInnerHTML={{ __html: menuItem.text }}
											/>
										</Link>
										{menuItem.subitems.length > 0 && (
											<>
												<button onClick={() => handleOpenSubmenuHeader(index)}>
													<BsChevronDown />
												</button>

												<ul className={`submenu-${index} unstyled-list p-0`}>
													{menuItem.subitems.map((submenuItem, index) => (
														<li key={index} className="d-block w-100">
															<Link href={submenuItem.href}>
																<a
																	className="d-block py-3 px-4"
																	dangerouslySetInnerHTML={{
																		__html: submenuItem.text,
																	}}
																/>
															</Link>
														</li>
													))}
												</ul>
											</>
										)}
									</ItemMenu>
								))}
							</MenuList>

							{/* Search bar */}
							<SearchBar className="d-none d-lg-block" isDesktop />

							{/* TODO: Convert label to i18n text */}
							<SidebarButton
								className="d-lg-none"
								onClick={() => controlSidebar.openMobileMenu()}
								aria-label="Open the site navigation menu"
								aria-haspopup={true}
								aria-controls="MobileMenuComponent"
								aria-expanded={controlSidebar.isOpen}
							>
								<span className="icon-menu" />
							</SidebarButton>
						</div>

						{/* Search mobile */}
						<div className="d-flex d-lg-none order-3">
							<SidebarButton
								onClick={() => controlSearchMenu.toggleSearchMenu()}
								aria-label="Open the site navigation menu"
							>
								{controlSearchMenu.isSearchMenuOpen ? (
									<span className="icon-close" />
								) : (
									<span className="icon-search" />
								)}
							</SidebarButton>
						</div>
					</HeaderContent>

					<MobileMenu menus={menu} />
					<SearchMenu />
				</Container>
			</Component>
		</>
	);
};
