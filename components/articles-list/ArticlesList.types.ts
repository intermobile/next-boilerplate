import { ArticleProps } from '@/typings/ArticleProps';

export interface ArticlesListProps {
	className?: string;
	posts: ArticleProps[];
	title?: string;
}
