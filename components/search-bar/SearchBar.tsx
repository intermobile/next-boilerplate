// External dependencies
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { Col, Row } from 'react-bootstrap';
import { RiSearch2Line } from 'react-icons/ri';

// Internal dependencies
import { FormSearch } from './SearchBar.styles';
import { SearchBarProps } from './SearchBar.types';

// Props for component functionality and logic
interface Props extends SearchBarProps {
	className?: string;
}

export const SearchBar: React.FC<Props> = ({ className = '', isDesktop, onSearch }) => {
	const [searchContent, setSearchContent] = useState('');
	const router = useRouter();

	const handleSubmitSearch = (event) => {
		event.preventDefault();
		if (searchContent !== '') {
			const searchedTerm = searchContent.replaceAll(' ', '+');
			router.push(`/search/?q=${searchedTerm}`);
		}

		onSearch && onSearch();
	};

	return (
		<FormSearch
			$isDesktop={isDesktop}
			className={className}
			onSubmit={(e) => handleSubmitSearch(e)}
			onKeyDown={(e) => {
				if (e.key === 'Enter') {
					handleSubmitSearch(e);
				}
			}}
		>
			<Row className="px-0 mx-0 position-relative">
				<Col className="px-0" xs={12}>
					{/* TODO: Convert to i18n text */}
					<input
						value={searchContent}
						onChange={(e) => setSearchContent(e.currentTarget.value)}
						type="text"
						placeholder="Looking for something special?"
					/>
				</Col>
				<div className="px-0 position-absolute search-icon">
					<button type="submit" className="pb-2">
						<span className="icon-search" />
					</button>
				</div>
			</Row>
		</FormSearch>
	);
};
