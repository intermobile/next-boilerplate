// Props for component data
export interface SearchBarProps {
	isDesktop?: boolean;
	onSearch?: () => void;
}
