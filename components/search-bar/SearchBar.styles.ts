import styled, { css } from 'styled-components';

interface SearchBarProps {
	$isDesktop: boolean;
}

export const FormSearch = styled.form<SearchBarProps>`
	margin: 0 auto;
	background-color: white;
	border: 1px solid ${({ theme }) => theme.colors.medium};
	border-radius: 5px;

	&.w-90 {
		width: 90%;
	}

	input {
		border: none;
		width: 100%;
		padding: 12px 25px;
		padding-right: 60px;
		height: 100%;
		border-radius: 5px;
	}

	button {
		width: 100%;
		height: 100%;
		background: none;
		border: none;
		text-align: center;
		font-size: 25px;
		border-radius: 5px;
	}

	.search-icon {
		right: 10px;
		width: 35px;

		span {
			display: block;
			margin-top: 9px;
		}
	}

	${(props) =>
		props.$isDesktop &&
		css`
			width: 330px;
		`}

	@media (max-width: 992px) {
		border-color: ${({ theme }) => theme.colors.black};

		button {
			color: ${({ theme }) => theme.colors.black};
		}

		input {
			padding-left: 10px;

			&::placeholder {
				font-size: 14px;
			}
		}
	}
`;
