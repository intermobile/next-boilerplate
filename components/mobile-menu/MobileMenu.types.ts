import { MenuProps } from '@/typings/MenuProps';

// Props for data and info
export interface MobileMenuProps {
	menus: MenuProps;
}
