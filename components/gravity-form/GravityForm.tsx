// External dependencies
import React from 'react';

// Internal dependencies
import { Component } from './GravityForm.styles';

// Types
interface Props {
	className?: string;
}

export const GravityForm = ({ className = '' }: Props): JSX.Element => {
	return <Component className={`${className}`}></Component>;
};
