import styled from 'styled-components';

export const Component = styled.div`
	background-color: #ffe66b;
	position: fixed;
	left: 0;
	bottom: 0;
	z-index: 99999;
	font-size: 0.9em;
`;

export const Button = styled.button`
	background-color: blue;
	color: white;
	border-radius: 6px;
`;
