// External dependencies
import React from 'react';
import { FaEye } from 'react-icons/fa';
import { Container } from 'react-bootstrap';

// Internal dependencies
import { Component } from './PreviewBanner.styles';
import { PreviewBannerProps } from './PreviewBanner.types';

// Props for component functionality and logic
interface Props extends PreviewBannerProps {
	className?: string;
}

export const PreviewBanner: React.FC<Props> = ({ className = '' }) => {
	return (
		<Component className={`${className} w-100`}>
			<Container className="py-3">
				<p className="d-flex flex-column flex-lg-row justify-content-start justify-content-lg-center text-center mb-0">
					<span className="d-inline-flex align-items-center mb-1 mb-lg-0">
						<FaEye />
						<b className="mx-1">Preview mode:</b>
					</span>
					<span className="d-inline-flex">
						The post content reflects how it is currently saved in the CMS.
					</span>
				</p>
			</Container>
		</Component>
	);
};
