# Next Boilerplate

## Requriements

-  [Node.js](https://nodejs.org/) - `10.13`+
-  [Plop](https://plopjs.com/) - Installed globally
-  [Gulp](https://gulpjs.com/) - Installed globally

## Getting Started

### 1. Install dependencies

```shell
npm install
```

### 2. Set API URL

Create a `.env.local` file at the root directory with:

```bash
NEXT_PUBLIC_WP_URL=""
```

The value for both should be the base URL for your API.

> Example: https://dev.intermobile.com.br/wp-headless-boilerplate

### 3. Start local server

```shell
npm run dev
```
