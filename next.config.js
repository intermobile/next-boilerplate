const colors = require('colors');
const path = require('path');
const fs = require('fs');
// const market = process.env.NEXT_PUBLIC_SITE_SLUG; // FOR MULTISITE
const cacheFilePath = process.env.CACHE_FILE_PATH;

module.exports = {
	trailingSlash: true,
	basePath: '',
	sassOptions: {
		includePaths: [path.join(__dirname, 'styles/scss/core')],
	},
	images: {
		domains: ['dev.intermobile.com.br', 'wp-headless-boilerplate.local'],
	},
	webpack: (config, { isServer }) => {
		if (isServer) {
			// Update cached data
			if (cacheFilePath && fs.existsSync(cacheFilePath)) {
				fs.rmSync(cacheFilePath);
				console.log(`${colors.gray('log  ')} - Local cache cleared`);
			}
			// Generates a sitemap.xml
			if (process.env.NODE_ENV !== 'development') {
				require('./utils/helpers/sitemap')();
			}
		}
		return config;
	},
};
