// External dependencies
import React from 'react';
import { Container } from 'react-bootstrap';
import Breadcrumbs from '@/components/breadcrumbs';

// Internal modules
import { Layout } from './DefaultLayout.styles';
import { LayoutProps } from '@/typings/LayoutProps';
import { DefaultLayoutContentProps } from './DefaultLayout.types';

export const DefaultLayout: React.FC<LayoutProps> = (props) => {
	const content = props.route.content as DefaultLayoutContentProps;

	return (
		<Layout {...props}>
			<Breadcrumbs items={props.route.breadcrumbs} current={props.route.title} />
			<Container>
				<strong>Default</strong> Layout
				<h1>{props.route.title}</h1>
				<pre>{JSON.stringify(content, null, '\t')}</pre>
			</Container>
		</Layout>
	);
};
