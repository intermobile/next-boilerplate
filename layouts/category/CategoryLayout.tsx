// External dependencies
import React from 'react';
import { Container } from 'react-bootstrap';

// Internal modules
import { Layout } from './CategoryLayout.styles';
import { LayoutProps } from '@/typings/LayoutProps';
import { CategoryLayoutContentProps } from './CategoryLayout.types';
import Breadcrumbs from '@/components/breadcrumbs';

export const CategoryLayout: React.FC<LayoutProps> = (props) => {
	const content = props.route.content as CategoryLayoutContentProps;

	return (
		<Layout {...props}>
			<Breadcrumbs items={props.route.breadcrumbs} current={props.route.title} />
			<Container>
				<strong>Category</strong> Layout
				<h1>{props.route.title}</h1>
				<pre>{JSON.stringify(content, null, '\t')}</pre>
			</Container>
		</Layout>
	);
};
