import { LinkProps } from '@/typings/LinkProps';

export interface CategoryLayoutContentProps {
	id: number;
	description: string;
	subcategories: LinkProps[];
}
