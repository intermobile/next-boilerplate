// External dependencies
import React from 'react';
import { Container } from 'react-bootstrap';
import Breadcrumbs from '@/components/breadcrumbs';

// Internal modules
import { Layout } from './PostLayout.styles';
import { LayoutProps } from '@/typings/LayoutProps';
import { PostLayoutContentProps } from './PostLayout.types';

export const PostLayout: React.FC<LayoutProps> = (props) => {
	const content = props.route.content as PostLayoutContentProps;

	return (
		<Layout {...props}>
			<Breadcrumbs items={props.route.breadcrumbs} current={props.route.title} />
			<Container>
				<strong>Post</strong> Layout
				<h1>{props.route.title}</h1>
				<pre>{JSON.stringify(content, null, '\t')}</pre>
			</Container>
		</Layout>
	);
};
