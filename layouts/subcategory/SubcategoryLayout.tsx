// External dependencies
import React from 'react';
import { Container } from 'react-bootstrap';

// Internal modules
import { Layout } from './SubcategoryLayout.styles';
import { LayoutProps } from '@/typings/LayoutProps';
import { SubcategoryLayoutContentProps } from './SubcategoryLayout.types';
import Breadcrumbs from '@/components/breadcrumbs';

export const SubcategoryLayout: React.FC<LayoutProps> = (props) => {
	const content = props.route.content as SubcategoryLayoutContentProps;

	return (
		<Layout {...props}>
			<Breadcrumbs items={props.route.breadcrumbs} current={props.route.title} />
			<Container>
				<strong>Subcategory</strong> Layout
				<h1>{props.route.title}</h1>
				<pre>{JSON.stringify(content, null, '\t')}</pre>
			</Container>
		</Layout>
	);
};
