import { ArticleProps } from '@/typings/ArticleProps';

export interface SubcategoryLayoutContentProps {
	description: string;
	articles: ArticleProps[];
}
