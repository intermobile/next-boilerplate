import { Site } from '@/config/site';

type PreviewContentType = 'post' /*  | 'page' */;

export interface PreviewProps {
	id: number;
	type: PreviewContentType;
	draft: boolean;
}

/**
 * Retrieve data for preview setup
 * @param id ID of the content to retrieve
 * @param type Type of content to retrieve
 * @param token Token to validate the request
 */
export async function getPreviewData(
	id: number,
	type: string = 'post',
	token: string,
): Promise<PreviewProps> {
	const fields = 'id,title,slug,path';
	const endpointsPerType = {
		post: 'posts',
		page: 'pages',
	};

	const endpoint = `${Site.apiBaseUrl}/wp/v2/${
		endpointsPerType[type]
	}/?_fields=${fields}&include=${id}&preview=true&token=${encodeURIComponent(token)}`;
	let postsData;

	try {
		const res = await fetch(endpoint);
		postsData = await res.json();
	} catch (err) {
		throw new Error(`Unable to fetch posts data from ${endpoint}\n` + err);
	}

	if (postsData.code || !postsData.length) return null;

	const post = postsData[0];
	return {
		id: post.id,
		draft: post.status === 'draft',
		type: Object.hasOwnProperty.call(endpointsPerType, type) ? type : '',
	} as PreviewProps;
}

/**
 * Returns an HTML string with the message to display
 * @param title Title of the page
 * @param message Message content
 */
export function getInfoPageHtml(title: string, message: string): string {
	return `<html>
	<head>
		<title>${title} | ${Site.title}</title>
		<style>
		* {
			font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
			margin-top: 0;
		}
		body {
			display: flex;
			align-items: center;
			justify-content: center;
			flex-direction: column;
			height: 100vh;
		}
		</style>
	</head>
	<body>
		<h1>${title}</h1>
		<p>${message}</p>
	</body>
</html>`;
}
