import { RouteProps } from '@/typings/RouteProps';
import { formatHomePageContent } from '@/mappings/home';
import { formatSeoDataFromYoastSEO } from '@/mappings/seo';
import { DefaultLayoutContentProps } from '@/layouts/default/DefaultLayout.types';
import { formatBreadcrumbsData } from '@/mappings/breadcrumbs';

/**
 * Maps page data from JSON to the Page object type
 * @param data Page JSON data
 */
export function formatPageRouteData(data: any): RouteProps {
	const route: RouteProps = {
		title: data.title?.rendered,
		path: data.path,
		seo: formatSeoDataFromYoastSEO(data.yoast_head),
		layout: data.layout,
		breadcrumbs: formatBreadcrumbsData(data.page_data.breadcrumbs),
		content: (() => {
			switch (data.layout) {
				case 'home':
					return formatHomePageContent(data);
				// PLOP PLACEHOLDER (DO NOT REMOVE)
				default:
					return formatDefaultPageContent(data);
			}
		})(),
	};

	return route;
}

/**
 * Maps page data from API to the layout content structure
 * @param data Page JSON data
 */
export function formatDefaultPageContent(data: any): DefaultLayoutContentProps {
	const content: DefaultLayoutContentProps = {
		htmlContent: data.content.rendered,
	};

	return content;
}
