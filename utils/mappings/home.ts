import { HomeLayoutContentProps } from 'layouts/home/HomeLayout.types';

/**
 * Maps page data from API to the layout content structure
 * @param data Page JSON data
 */
export function formatHomePageContent(data: any): HomeLayoutContentProps {
	const content: HomeLayoutContentProps = {
		latestArticles: data.page_data.posts,
	};

	return content;
}
