import { PostLayoutContentProps } from '@/layouts/post/PostLayout.types';
import { ArticleProps } from '@/typings/ArticleProps';
import { RouteProps } from '@/typings/RouteProps';
import { formatImageData } from '@/mappings/image';
import { formatSeoDataFromYoastSEO } from '@/mappings/seo';
import { formatBreadcrumbsData } from '@/mappings/breadcrumbs';
/**
 * Maps post data from JSON to the RouteProps object type for article pages
 * @param data Post JSON data
 */
export function formatPostRouteData(data): RouteProps {
	const layoutData: RouteProps = {
		title: data.title?.rendered,
		path: data.path,
		seo: formatSeoDataFromYoastSEO(data.yoast_head),
		layout: data.layout,
		breadcrumbs: formatBreadcrumbsData(data.page_data.breadcrumbs),
		content: {
			htmlContent: data.content.rendered,
			excerpt: data.extract,
			categories: data.categories,
			image: formatImageData(data.image, 'large'),
		} as PostLayoutContentProps,
	};

	return layoutData;
}

/**
 * Maps data from JSON for posts listing
 * @param data Array with post JSON data
 */
export function formatPosts(data: any[]): ArticleProps[] {
	const posts: ArticleProps[] = [];

	data.forEach((postData) => {
		const post: ArticleProps = {
			id: postData.id,
			title: postData.title?.rendered,
			slug: postData.slug,
			path: postData.path,
		};
		if (postData.excerpt) {
			post.excerpt = postData.excerpt;
		}
		if (postData.categories) {
			post.categories = postData.categories;
		}
		if (postData.primary_category) {
			post.primaryCategory = postData.primary_category;
		}
		if (postData.image) {
			post.image = formatImageData(postData.image, 'medium');
		}
		posts.push(post);
	});
	return posts;
}
