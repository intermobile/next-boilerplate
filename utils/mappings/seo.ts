import { Site } from '@/config/site';
import { SeoProps } from '@/typings/SeoProps';

/**
 * Maps data from JSON to the SEO object type
 * @param yoastHtml HTML content from Yoast SEO for the page <head>
 */
export function formatSeoDataFromYoastSEO(yoastHtml: string): SeoProps {
	let headHtml = yoastHtml;

	// Extract meta title
	const metaTitle = headHtml.match(/(?<=og:title"\scontent=")(.+?)(?=")/)[0];

	// Add self-referencig canonical if not set
	const hasCanonical = new RegExp(/rel="canonical"/).test(headHtml);
	if (!hasCanonical) {
		const currentUrl = headHtml.match(/(?<=og:url"\scontent=")(.+?)(?=")/)[0];
		const canonicalHtmlTag = `<link rel="canonical" href="${currentUrl}">`;
		headHtml = headHtml.replace('\n<meta', `\n${canonicalHtmlTag}\n<meta`);
	}

	// Replace the default search URL
	headHtml = headHtml.replace(/\/\?s={search_term_string}/g, '/search/{search_term_string}/');

	// Replace CMS URLs by website's
	headHtml = headHtml.replace(
		/(https?):\/\/(?:(?:cms.next-boilerplate.com)|(?:dev.greenparkcontent.com\/next-boilerplate))\/(.*?)"/g,
		`${Site.baseUrl}/$2"`,
	);

	// Revert media URLs for the CMS
	headHtml = headHtml.replace(
		/(https?):\/\/next-boilerplate\.com\/wp-content\//g,
		'$1://cms.next-boilerplate.com/wp-content/',
	);

	// Extract schema JSON
	const schemaMatch = headHtml.match(/<script type="application\/ld\+json".*?>(.*?)<\/script>\n/);
	const schemaJson = schemaMatch[1] ? JSON.parse(schemaMatch[1]) : undefined;
	headHtml = headHtml.replace(schemaMatch[0], '');

	const seo: SeoProps = {
		title: metaTitle,
		headHtml,
		schemaJson,
	};

	return seo;
}
