import { CategoryLayoutContentProps } from '@/layouts/category/CategoryLayout.types';
import { CategoryProps } from '@/typings/CategoryProps';
import { LinkProps } from '@/typings/LinkProps';
import { RouteProps } from '@/typings/RouteProps';
import { formatSeoDataFromYoastSEO } from './seo';
import { formatBreadcrumbsData } from '@/mappings/breadcrumbs';

/**
 * Maps category data from JSON to the RouteProps object type for category pages
 * @param data Category JSON data
 */
export function formatCategoryRouteData(data: any): RouteProps {
	const layoutData: RouteProps = {
		title: data.name,
		path: data.path,
		seo: formatSeoDataFromYoastSEO(data.yoast_head),
		layout: data.layout,
		breadcrumbs: formatBreadcrumbsData(data.page_data.breadcrumbs),
		content: {
			description: data.description,
			subcategories: data.page_data.subcategories.map((subcategory) => {
				return {
					text: subcategory.name,
					href: subcategory.path,
				} as LinkProps;
			}),
		} as CategoryLayoutContentProps,
	};

	return layoutData;
}

/**
 * Maps category data from JSON to the CategoryProps object type for category listing
 * @param data Category JSON data
 */
export function formatCategoriesData(data: any[]): CategoryProps[] {
	const categories: CategoryProps[] = [];
	data.forEach((categoryData) => {
		const category: CategoryProps = {
			id: categoryData.id,
			name: categoryData.name,
			slug: categoryData.slug,
			path: categoryData.path,
		};
		categories.push(category);
	});
	return categories;
}
