import { LinkProps } from '@/typings/LinkProps';

/**
 * Return formatted Breadcrumbs data
 * @param dataArray Breadcrumbs data from API. It should follow the ACF data properties
 */
export function formatBreadcrumbsData(dataArray): LinkProps[] {
	if (dataArray && Array.isArray(dataArray)) {
		const breadcrumbs = [];
		dataArray.forEach((item) => {
			breadcrumbs.push({
				text: item.title || item.name,
				href: item.path,
			} as LinkProps);
		});
		return breadcrumbs;
	} else {
		return [];
	}
}
