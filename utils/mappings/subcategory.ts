import { SubcategoryLayoutContentProps } from '@/layouts/subcategory/SubcategoryLayout.types';
import { RouteProps } from '@/typings/RouteProps';
import { formatSeoDataFromYoastSEO } from './seo';
import { formatBreadcrumbsData } from '@/mappings/breadcrumbs';

/**
 * Maps category data from JSON to the RouteProps object type for category pages
 * @param data Category JSON data
 */
export function formatSubcategoryRouteData(data: any): RouteProps {
	const layoutData: RouteProps = {
		title: data.name,
		path: data.path,
		seo: formatSeoDataFromYoastSEO(data.yoast_head),
		layout: data.layout,
		breadcrumbs: formatBreadcrumbsData(data.page_data.breadcrumbs),
		content: {
			description: data.description,
			articles: data.posts, // Already formatted to ArticleProps[]
		} as SubcategoryLayoutContentProps,
	};

	return layoutData;
}
