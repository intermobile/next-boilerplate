import { Site } from '@/config/site';
import { RouteProps } from '@/typings/RouteProps';
import { WpQueryArgs } from '@/typings/WpQueryArgs';
import { trimPathSlashes } from '@/helpers/string';
import { getDataFromWordpressApi } from '@/data/queries';
import { getPostRouteData } from '@/data/posts';
import { getPageRouteData } from '@/data/pages';
import { getCategoryRouteData } from '@/data/categories';
import { getLocalCacheData } from './cache';
import { getSubcategoryRouteData } from './subcategories';

export type Endpoint = 'posts' | 'pages' | 'categories';

/**
 * Retrieves a list with the path from all posts from the API
 * @param routeType The endpoint to fetch data from
 */
export async function getAllPathsFromRouteType(routeType: Endpoint): Promise<string[]> {
	const args: WpQueryArgs = {
		perPage: -1,
		_fields: ['count', 'path', 'is_front_page'],
	};
	if (routeType === 'categories') {
		args.hideEmpty = true;
		args.exclude = [1];
	}
	const data = await getDataFromWordpressApi(`/wp/v2/${routeType}`, args);

	// Build the paths list
	const paths = [];
	data
		.filter((route) => !route.is_front_page) // Ignore Homepage
		.forEach((route) => {
			const perPage = Site.postsPerPage;
			const path = trimPathSlashes(route.path).split('/');

			// Add route path
			paths.push(path);

			// Add route pagination paths
			if ('count' in route && route.count > perPage && path.length === 2) {
				const totalPages = Math.ceil(route.count / Site.postsPerPage);

				for (let i = 2; i <= totalPages; i++) {
					paths.push([...path, String(i)]);
				}
			}
		});

	return paths;
}

/**
 * Retrieve content from a page, post or category from the API
 * This function expects the post URL to include the category/subcategory slug(s)
 * @param path Full path of the page to retrieve.
 */
export async function getDynamicRouteData(path: string[]): Promise<RouteProps> {
	if (!path.length) {
		return getPageRouteData([]);
	}

	// Get categories slugs from local cache
	const allCategoriesSlugs = getLocalCacheData().categoriesSlugs;
	const routeSlug = path[path.length - 1];

	// Check if the first path is a category slug
	if (allCategoriesSlugs.includes(path[0])) {
		if (path.length === 1) {
			// CATEGORY
			return getCategoryRouteData(routeSlug);
		} else if (path.length === 2 && allCategoriesSlugs.includes(path[1])) {
			// SUBCATEGORY
			return getSubcategoryRouteData(path[1], 1);
		} else if (Number(path[2]) && allCategoriesSlugs.includes(path[1])) {
			// SUBCATEGORY
			return getSubcategoryRouteData(path[1], Number(path[2]));
		} else if (path.length === 3) {
			// POST
			return getPostRouteData(routeSlug);
		} else {
			throw new Error(`Unexpected route pattern /${path.join('/')}/.`);
		}
	} else {
		// PAGE
		return getPageRouteData(path);
	}
}
