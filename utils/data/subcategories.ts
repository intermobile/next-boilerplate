import { RouteProps } from '@/typings/RouteProps';
import { formatSubcategoryRouteData } from '@/mappings/subcategory';
import { getDataFromWordpressApi } from '@/data/queries';
import { Site } from '@/config/site';
import { getPostsByCategory } from './posts';

/**
 * Retrieve data for the subcategory route from the API
 * @param slug Slug of the subategory to retrieve
 */
export async function getSubcategoryRouteData(slug: string, page?: number): Promise<RouteProps> {
	const data = await getDataFromWordpressApi('/wp/v2/categories', {
		slug: slug,
		_fields: ['id', 'name', 'description', 'layout', 'path', 'parent', 'page_data', 'yoast_head'],
	});
	const categoryData = data[0];

	// Get posts from category
	const posts = await getPostsByCategory(categoryData.id, Site.postsPerPage, {
		page: page || 1,
	});

	return formatSubcategoryRouteData({ ...categoryData, posts });
}
