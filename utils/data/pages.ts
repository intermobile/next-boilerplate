import { trimPathSlashes } from '@/helpers/string';
import { formatPageRouteData } from '@/mappings/page';
import { RouteProps } from '@/typings/RouteProps';
import { SitemapRouteProps } from '@/typings/SitemapRouteProps';
import { getPosts } from './posts';
import { getDataFromWordpressApi } from './queries';

/**
 * Retrieves a page from the API
 * @param path Path of the page to retrieve. Use [] (empty array) to get homepage
 */
export async function getPageRouteData(path: string[]): Promise<RouteProps> {
	const slug = path.length ? path[path.length - 1] : 'home';
	const data = await getDataFromWordpressApi('/wp/v2/pages', {
		slug: slug,
		_fields: [
			'id',
			'title',
			'slug',
			'path',
			'content',
			'is_front_page',
			'layout',
			'page_data',
			'yoast_head',
		],
	});

	// Try to get the page that matches the exact path
	const page = data.find((page) => {
		const permalink = trimPathSlashes(page.path);
		return slug === 'home' ? permalink === '' : permalink === path.join('/');
	});

	// Get additional data per layout type
	if (page.layout === 'home') {
		page.page_data.posts = await getPosts(6);
	}

	return formatPageRouteData(page);
}

/**
 * Retrieves a list with all pages for sitemap listing
 */
export async function getSitemapDataFromAllPosts(): Promise<SitemapRouteProps[]> {
	const data = await getDataFromWordpressApi(`/wp/v2/pages`, {
		_fields: ['title', 'path'],
		perPage: -1,
		order: 'asc',
		orderby: 'title',
	});

	const pages: SitemapRouteProps[] = data.map((page) => {
		return {
			title: page.title.rendered,
			path: page.path,
		} as SitemapRouteProps;
	});

	return pages;
}
