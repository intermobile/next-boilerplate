import { getDataFromWordpressApi } from '@/data/queries';
import { formatCategoryRouteData } from '@/mappings/category';
import { RouteProps } from '@/typings/RouteProps';
import { SitemapRouteProps } from '@/typings/SitemapRouteProps';

/**
 * Retrieve data for the category route from the API
 * @param slug Slug of the category to retrieve
 */
export async function getCategoryRouteData(slug: string): Promise<RouteProps> {
	const data = await getDataFromWordpressApi('/wp/v2/categories', {
		slug: slug,
		_fields: ['id', 'name', 'description', 'layout', 'path', 'parent', 'page_data', 'yoast_head'],
	});

	return formatCategoryRouteData(data[0]);
}

/**
 * Retrieves a list with the slug from all categories
 */
export async function getSlugFromAllCategories(): Promise<string[]> {
	const data: any = await getDataFromWordpressApi(`/wp/v2/categories`, {
		_fields: ['slug', 'count'], // Count is needed for the API to properly hide empty categories
		perPage: -1,
		exclude: [1],
		hideEmpty: true,
	});
	return data.map((category) => category.slug);
}

/**
 * Retrieves a list with all categories for sitemap listing
 */
export async function getSitemapDataFromAllCategories(): Promise<SitemapRouteProps[]> {
	const data = await getDataFromWordpressApi(`/wp/v2/categories`, {
		_fields: ['id', 'name', 'path', 'parent', 'count'],
		perPage: -1,
		order: 'asc',
		orderby: 'title',
		hideEmpty: true,
		exclude: [1],
	});

	const categories: SitemapRouteProps[] = data.map((category) => {
		return {
			id: category.id,
			title: category.name,
			path: category.path,
			parent: category.parent,
		} as SitemapRouteProps;
	});

	return categories;
}
