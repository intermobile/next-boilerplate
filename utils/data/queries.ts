import { snakeCase } from 'snake-case';
import { Site } from '@/config/site';
import { clamp } from '@/helpers/math';
import { WpQueryArgs } from '@/typings/WpQueryArgs';

/**
 * Retrieve data from WordPress api.
 * @param endpoint Endpoint URL without query params (i.e. /wp/v2/posts).
 * @param args Query options to be passed to the API. Set perPage to -1 to get all results.
 */
export async function getDataFromWordpressApi(endpoint: string, args?: WpQueryArgs): Promise<any> {
	return new Promise(async (resolve) => {
		// Quantity is 10 if not defined, and is limited to 100
		const queryArgs: WpQueryArgs = args ? { ...args } : {};
		const quantity = queryArgs.perPage ? clamp(queryArgs.perPage, -1, 100) : 10;
		if (quantity === -1) {
			queryArgs.perPage = 100;
		}

		if (args && args.id) {
			// Set endpoint for object instead of list
			endpoint += `/${args.id}`;
		}

		let url = Site.apiBaseUrl + endpoint + wpQueryArgsToString(queryArgs);
		let data;

		try {
			let res = await fetch(url);
			data = await res.json();

			if (quantity === -1) {
				const pagesTotal = parseInt(res.headers.get('x-wp-totalpages'));

				// Get more items, due to the limit of 100 items per page from the WP API
				if (pagesTotal > 1) {
					for (let i = 2; i <= pagesTotal; i++) {
						queryArgs.page = i;
						url = Site.apiBaseUrl + endpoint + wpQueryArgsToString(queryArgs);
						res = await fetch(url);
						const moreData = await res.json();
						data = [...data, ...moreData];
					}
				}
			}
			resolve(data);
		} catch (err) {
			throw new Error(`Unable to fetch data from ${url}\n` + err);
		}
	});
}

/**
 * Returns the query args as a string for using in the API requests
 * @param args Args for the query
 */
export function wpQueryArgsToString(args: WpQueryArgs): string {
	let queryParams = '?';

	// Add each property as query param
	for (const arg in args) {
		const key = arg === '_fields' ? arg : snakeCase(arg);
		const value = args[arg];

		if (Array.isArray(value)) {
			if (value.length > 0) {
				queryParams += `${key}=${value.join(',')}&`;
			}
		} else {
			if (value !== '') {
				queryParams += `${key}=${value}&`;
			}
		}
	}
	return queryParams.replace(/[&?]$/, ''); // Remove trailing ampersand (&)
}
