import { OptionsProps } from '@/typings/OptionsProps';
import { formatSiteOptions } from '@/mappings/options';
import { MenuProps } from '@/typings/MenuProps';
import { formatSiteMenus } from '@/mappings/menu';
import { getDataFromWordpressApi } from './queries';

/**
 * Retrieve object with general site options and configurations
 */
export async function getSiteOptions(): Promise<OptionsProps> {
	const data = await getDataFromWordpressApi('/site/v1/options');
	return formatSiteOptions(data);
}

/**
 * Retrieve object with general site options and configurations
 */
export async function getSiteMenus(): Promise<MenuProps[]> {
	const data = await getDataFromWordpressApi('/site/v1/menus');
	return formatSiteMenus(data);
}
