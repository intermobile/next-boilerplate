import colors from 'colors';
import { Site } from '@/config/site';
import { CacheProps } from '@/typings/CacheProps';
import { readFileSync, writeFileSync, existsSync } from 'fs';
import { getSlugFromAllCategories } from '@/data/categories';
import { getAllPathsFromRouteType } from '@/data/paths';
import { getSiteMenus, getSiteOptions } from '@/data/site';

/**
 * Generates a local cache JSON for common data, only if not already generated
 */
export async function prepareLocalCache(): Promise<void> {
	// Check if file already exists
	if (!existsSync(Site.cacheFilePath)) {
		const cacheData = {
			categoriesSlugs: await getSlugFromAllCategories(),
			options: await getSiteOptions(),
			menus: await getSiteMenus(),
			paths: {
				posts: await getAllPathsFromRouteType('posts'),
				pages: await getAllPathsFromRouteType('pages'),
				categories: await getAllPathsFromRouteType('categories'),
			},
		};
		writeFileSync(Site.cacheFilePath, JSON.stringify(cacheData, null, '\t'));

		process.stdout.write('\r\x1b[K'); // Erase the current line in the terminal
		console.log(`${colors.gray('log  ')} - Local cache generated`);
	}
}

/**
 * Retrieve the common data from the local cached JSON
 */
export function getLocalCacheData(): CacheProps {
	if (!existsSync(Site.cacheFilePath)) {
		return null;
	}
	const cache = readFileSync(Site.cacheFilePath);
	return JSON.parse(cache.toString());
}

/**
 * Retrieve the common data from the local cached JSON
 */
export async function fetchPublicCacheData(siteUrl: string): Promise<CacheProps> {
	return new Promise(async (resolve, reject) => {
		try {
			//const path = `${siteUrl}/${Site.slug}${Site.cacheFilePath.replace(/^\.\/public/, '')}`; // FOR MULTISITE
			const path = `${siteUrl}${Site.cacheFilePath.replace(/^\.\/public/, '')}`;
			const cacheRes = await fetch(path);
			resolve(await cacheRes.json());
		} catch (e) {
			reject(e);
		}
	});
}
