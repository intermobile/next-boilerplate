import { ArticleProps } from '@/typings/ArticleProps';
import { RouteProps } from '@/typings/RouteProps';
import { WpQueryArgs } from '@/typings/WpQueryArgs';
import { SitemapRouteProps } from '@/typings/SitemapRouteProps';
import { formatPostRouteData, formatPosts } from '@/mappings/post';
import { getDataFromWordpressApi } from '@/data/queries';

/**
 * Retrieve data from the API for a post route
 * @param post Slug or ID of the post to retrieve. If number is provided, it will consider draft posts.
 * @param token Token to preview draft posts
 */
export async function getPostRouteData(
	post: string | number,
	token: string = undefined,
): Promise<RouteProps> {
	const isPreview = typeof post === 'number';

	const args: WpQueryArgs = {
		_fields: [
			'id',
			'title',
			'slug',
			'date',
			'extract',
			'categories',
			'content',
			'image',
			'layout',
			'path',
			'page_data',
			'yoast_head',
		],
	};

	if (isPreview) {
		args.include = [post];
		args.preview = true;
		args.token = encodeURIComponent(token);
	} else {
		args.slug = post;
	}

	const data = await getDataFromWordpressApi('/wp/v2/posts', args);

	return formatPostRouteData(data[0]);
}

/**
 * Retrieves a list of posts from the API
 * @param quantity Number of posts to retrieve, default is 12. Use -1 to get all posts
 * @param args Optional args to the query
 */
export async function getPosts(quantity = 12, args: WpQueryArgs = {}): Promise<ArticleProps[]> {
	const data = await getDataFromWordpressApi('/wp/v2/posts', {
		_fields: [
			'id',
			'title',
			'slug',
			'path',
			'date',
			'excerpt',
			'category',
			'categories',
			'image',
		],
		perPage: quantity,
		...args,
	});

	return formatPosts(data);
}

/**
 * Retrieves a list of posts associated with a specific category
 * @param category Id of the category to get posts from
 * @param quantity Number of posts to retrieve, default is 12. Use -1 to get all posts
 * @param args Optional args to the query
 */
export async function getPostsByCategory(
	category: number,
	quantity = 12,
	args: WpQueryArgs = {},
): Promise<ArticleProps[]> {
	return await getPosts(quantity, {
		categories: [category],
		...args,
	});
}

/**
 * Retrieves a list with all posts for sitemap listing
 */
export async function getSitemapDataFromAllPosts(): Promise<SitemapRouteProps[]> {
	const data = await getDataFromWordpressApi(`/wp/v2/posts`, {
		_fields: ['title', 'path', 'id', 'category'],
		perPage: -1,
		order: 'asc',
		orderby: 'title',
	});

	const posts: SitemapRouteProps[] = data.map((post) => {
		return {
			id: post.id,
			title: post.title.rendered,
			path: post.path,
			parent: post.category,
		} as SitemapRouteProps;
	});

	return posts;
}
