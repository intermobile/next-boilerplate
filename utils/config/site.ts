// const siteSlug = process.env.NEXT_PUBLIC_SITE_SLUG; // FOR MULTISITE
const siteTitle = process.env.NEXT_PUBLIC_SITE_NAME;
const siteLang = process.env.NEXT_PUBLIC_SITE_LANG;
const siteUrl = process.env.NEXT_PUBLIC_SITE_URL;
// const apiBaseUrl = `${process.env.NEXT_PUBLIC_WP_URL}/${siteSlug}/wp-json`; // FOR MULTISITE
const apiBaseUrl = `${process.env.NEXT_PUBLIC_WP_URL}/wp-json`;

const gtmId = process.env.NEXT_PUBLIC_GTM_ID;
const gaId = process.env.NEXT_PUBLIC_GA_ID;
const isAnalyticsEnabled = process.env.NEXT_PUBLIC_IS_ANALYTICS_ENABLED === 'true';

const postRoutesLimit = process.env.LIMIT_POSTS ? +process.env.LIMIT_POSTS : undefined;
const pageRoutesLimit = process.env.LIMIT_PAGES ? +process.env.LIMIT_PAGES : undefined;
const categRoutesLimit = process.env.LIMIT_CATEGORIES ? +process.env.LIMIT_CATEGORIES : undefined;

const cacheFilePath = process.env.CACHE_FILE_PATH;

export class Site {
	// SITE INFO
	static title: string = siteTitle;
	// static slug: string = siteSlug; // FOR MULTISITE
	static lang: string = siteLang;
	static baseUrl: string = siteUrl;

	// PAGINATION
	static postsPerPage = 12;

	// API ENDPOINT
	static apiBaseUrl: string = apiBaseUrl;

	// ANALYTICS
	static gtmId: string = gtmId;
	static gaId: string = gaId;
	static isAnalyticsEnabled: boolean = isAnalyticsEnabled;

	// BUILD CACHE
	static cacheFilePath = cacheFilePath;

	// [...PAGE] LIMITS
	static limitPosts = postRoutesLimit;
	static limitPages = pageRoutesLimit;
	static limitCategories = categRoutesLimit;
}
