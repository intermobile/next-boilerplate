export interface SeoProps {
	title: string;
	headHtml?: string;
	schemaJson?: string;
}
