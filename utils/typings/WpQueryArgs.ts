export interface WpQueryArgs {
	_fields?: string[];
	id?: number;
	perPage?: number;
	page?: number;
	context?: 'view' | 'embed' | 'edit';
	search?: string;
	after?: string;
	author?: string;
	authorExclude?: string;
	before?: string;
	include?: number[];
	exclude?: number[];
	hideEmpty?: boolean;
	offset?: number;
	order?: 'asc' | 'desc';
	orderby?:
		| 'author'
		| 'date'
		| 'count'
		| 'description'
		| 'email'
		| 'id'
		| 'include'
		| 'include_slugs'
		| 'modified'
		| 'name'
		| 'parent'
		| 'registered_date'
		| 'relevance'
		| 'slug'
		| 'term_group'
		| 'title'
		| 'url';
	slug?: string;
	status?: string;
	taxRelation?: 'AND' | 'OR';
	categories?: number[];
	categoriesExclude?: number[];
	tags?: number[];
	tagsExclude?: number[];
	sticky?: boolean;
	roles?: string;
	who?: 'authors';
	preview?: boolean; // For draft preview
	token?: string; // For draft preview
}
