export interface OptionsProps {
	footer: {
		copyright: string;
	};
	mode: 'default' | 'preview';
}
