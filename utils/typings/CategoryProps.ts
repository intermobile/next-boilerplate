export interface CategoryProps {
	id: number;
	name: string;
	slug: string;
	path: string;
}
