import { MenuProps } from './MenuProps';
import { OptionsProps } from './OptionsProps';

export interface CacheProps {
	categoriesSlugs: string[];
	options: OptionsProps;
	menus: MenuProps[];
	paths: {
		posts: Array<string[]>;
		pages: Array<string[]>;
		categories: Array<string[]>;
	};
}
