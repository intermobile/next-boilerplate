import { ImageProps } from '@/typings/ImageProps';

export interface ArticleProps {
	id: number;
	slug: string;
	title: string;
	path: string;
	image?: ImageProps;
	date?: string;
	excerpt?: string;
	categories?: number[];
	primaryCategory?: number;
}
