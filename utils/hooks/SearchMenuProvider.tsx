import React, { useState, useCallback, createContext, useContext } from 'react';

interface SearchMenuContextProps {
	openSearchMenu(): void;
	closeSearchMenu(): void;
	toggleSearchMenu(): void;
	isSearchMenuOpen: boolean;
}

interface SearchMenuProviderProps {
	children: React.ReactNode;
}

const SearchMenuContext = createContext<SearchMenuContextProps>({} as SearchMenuContextProps);

export const SearchMenuProvider: React.FC<SearchMenuProviderProps> = ({ children }) => {
	const [isSearchMenuOpen, setIsSearchMenuOpen] = useState(false);

	const openSearchMenu = useCallback(() => {
		setIsSearchMenuOpen(true);
	}, []);

	const closeSearchMenu = useCallback(() => {
		setIsSearchMenuOpen(false);
	}, []);

	const toggleSearchMenu = useCallback(() => {
		setIsSearchMenuOpen((prevsOpen) => !prevsOpen);
	}, []);

	return (
		<SearchMenuContext.Provider
			value={{
				openSearchMenu,
				closeSearchMenu,
				toggleSearchMenu,
				isSearchMenuOpen,
			}}
		>
			{children}
		</SearchMenuContext.Provider>
	);
};

export function useSearchMenu(): SearchMenuContextProps {
	const context = useContext(SearchMenuContext);

	return context;
}
