import React from 'react';
import { MobileMenuProvider } from './MobileMenuProvider';
import { SearchMenuProvider } from './SearchMenuProvider';

export const AppProvider: React.FC = ({ children }) => {
	return (
		<SearchMenuProvider>
			<MobileMenuProvider>{children}</MobileMenuProvider>
		</SearchMenuProvider>
	);
};
